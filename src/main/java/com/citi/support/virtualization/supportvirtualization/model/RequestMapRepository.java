package com.citi.support.virtualization.supportvirtualization.model;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * <code>RequestMapRepository</code>.
 *
 * @author salvador
 * @version 1.0
 */
 @Transactional
 @Repository
public interface RequestMapRepository extends CrudRepository<RequestMap, String>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.data.repository.CrudRepository#findAll()
	 */
	//public List<RequestMap> findAll();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.data.repository.CrudRepository#deleteById(java.lang.
	 * Object)
	 */
	//public void deleteById(String id);

	/**
	 * Find by request value.
	 *
	 * @return list
	 */
	//public List<RequestMap> findByRequest(String request);
	
}
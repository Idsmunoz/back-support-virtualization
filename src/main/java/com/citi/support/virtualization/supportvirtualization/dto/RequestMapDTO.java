package com.citi.support.virtualization.supportvirtualization.dto;

import java.io.Serializable;

public class RequestMapDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/** id. */	
	private String id;
	
	/** request. */
	private String request;
	
	/** response. */
	private String response;
	
	/** request type. */
	private String requestType;
	
	/** dialog type. */
	private String dialogType;
	
	/** time out value. */
	private Integer timeOutValue;

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getDialogType() {
		return dialogType;
	}
	public void setDialogType(String dialogType) {
		this.dialogType = dialogType;
	}
	public Integer getTimeOutValue() {
		return timeOutValue;
	}
	public void setTimeOutValue(Integer timeOutValue) {
		this.timeOutValue = timeOutValue;
	}
	
	
	
}

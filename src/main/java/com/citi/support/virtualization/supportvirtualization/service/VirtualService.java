package com.citi.support.virtualization.supportvirtualization.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citi.support.virtualization.supportvirtualization.dto.RequestMapDTO;
import com.citi.support.virtualization.supportvirtualization.model.RequestMap;
import com.citi.support.virtualization.supportvirtualization.model.RequestMapRepository;


@Service
public class VirtualService {

	@Autowired
	private RequestMapRepository requestMapRepository;


	@Transactional(readOnly=true)
	public List<RequestMapDTO> findAll() {
		// TODO Auto-generated method stub
		
		
		List<RequestMapDTO> list = new ArrayList<>();
		Iterable<RequestMap> listRequestMap = requestMapRepository.findAll();
		
		for(RequestMap rm :listRequestMap) {
		
			RequestMapDTO requestMapDTO = new RequestMapDTO();
			requestMapDTO.setDialogType(rm.getDialogType());
			requestMapDTO.setId(rm.getId());
			requestMapDTO.setRequest(rm.getRequest());
			requestMapDTO.setRequestType(rm.getRequestType());
			requestMapDTO.setResponse(rm.getResponse());
			requestMapDTO.setTimeOutValue(rm.getTimeOutValue());
		
			list.add(requestMapDTO);			
		}
		
		return list;
	}

	@Transactional
	public void deleteById(String id) {
		// TODO Auto-generated method stub
		requestMapRepository.deleteById(id);
		
	}


	@Transactional
	public String save(RequestMapDTO requestMapDTO) {
		// TODO Auto-generated method stub
		
		RequestMap requestMap = new RequestMap();
		requestMap.setDialogType(requestMapDTO.getDialogType());
		requestMap.setId(requestMapDTO.getId());
		requestMap.setRequest(requestMapDTO.getRequest());
		requestMap.setRequestType(requestMapDTO.getRequestType());
		requestMap.setResponse(requestMapDTO.getResponse());
		requestMap.setTimeOutValue(requestMapDTO.getTimeOutValue());			

		
		if(requestMapRepository.save(requestMap)==null) {
			return "Error";
		}else {
			return "OK"; 
		}
		
		
	}

	@Transactional(readOnly=true)
	public List<RequestMapDTO> findOne(String id) {
		// TODO Auto-generated method stub
		RequestMap requestMap = requestMapRepository.findById(id).orElse(null);
		RequestMapDTO requestMapDTO = new RequestMapDTO();
		List<RequestMapDTO> list = new ArrayList<>();	
		if(requestMap != null) {
			requestMapDTO.setDialogType(requestMap.getDialogType());
			requestMapDTO.setId(requestMap.getId());
			requestMapDTO.setRequest(requestMap.getRequest());
			requestMapDTO.setRequestType(requestMap.getRequestType());
			requestMapDTO.setResponse(requestMap.getResponse());
			requestMapDTO.setTimeOutValue(requestMap.getTimeOutValue());
			list.add(requestMapDTO);
		}
		
	return list;
	}

}

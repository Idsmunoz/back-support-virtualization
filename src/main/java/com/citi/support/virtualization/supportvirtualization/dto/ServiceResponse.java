package com.citi.support.virtualization.supportvirtualization.dto;

public class ServiceResponse {

	private Object response;
	private ErrorServiceResponse error;
	
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	public ErrorServiceResponse getError() {
		return error;
	}
	public void setError(ErrorServiceResponse error) {
		this.error = error;
	}
	
	
	

	
}

package com.citi.support.virtualization.supportvirtualization.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="requestmap")
public class RequestMap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** id. */	
	@Id 
	@NotEmpty
	@Column(name = "request_mapping_id", length = 15)
	private String id;
	
	/** request. */
	@Column(name = "request_value", length = 3500)
	@NotNull @JsonProperty(value = "request_value")
	@NotEmpty
	private String request;
	
	/** response. */
	@Column(name = "response_value", length = 3500)
    @NotNull @JsonProperty(value = "response_value")
	private String response;
	
	/** request type. */
	@Column(name = "response_type", length = 7)
	@Pattern(regexp = "OK|NOK|TIMEOUT")
	@NotNull @JsonProperty(value = "request_type")
	private String requestType;
	
	/** dialog type. */
	@Column(name = "dialog_type", length = 4)
	@Pattern(regexp = "SA1|SA2|STD2")
    @NotNull @JsonProperty(value = "dialog_type")
	private String dialogType;
	
	/** time out value. */
	@Column(name = "timeout_value", length = 3)
	@JsonProperty(value = "timeout_in_seconds")
	@Min(0) @Max(300)
	private Integer timeOutValue;

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getDialogType() {
		return dialogType;
	}
	public void setDialogType(String dialogType) {
		this.dialogType = dialogType;
	}
	public Integer getTimeOutValue() {
		return timeOutValue;
	}
	public void setTimeOutValue(Integer timeOutValue) {
		this.timeOutValue = timeOutValue;
	}
	
	
	
}

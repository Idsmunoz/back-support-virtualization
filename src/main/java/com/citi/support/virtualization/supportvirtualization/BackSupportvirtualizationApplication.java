package com.citi.support.virtualization.supportvirtualization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackSupportvirtualizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackSupportvirtualizationApplication.class, args);
	}
}

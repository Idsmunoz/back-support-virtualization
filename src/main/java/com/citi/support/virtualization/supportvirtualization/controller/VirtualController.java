package com.citi.support.virtualization.supportvirtualization.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.citi.support.virtualization.supportvirtualization.dto.RequestMapDTO;
import com.citi.support.virtualization.supportvirtualization.dto.ServiceResponse;
import com.citi.support.virtualization.supportvirtualization.service.VirtualService;


@RestController
@RequestMapping(path="/supportVirtual")
public class VirtualController {

	
	@Autowired
	private VirtualService virtualService;
	
	@GetMapping(value="/all")
	public ServiceResponse getFindAll() {
		
		ServiceResponse out = new ServiceResponse();
		List<RequestMapDTO> response= virtualService.findAll();		
		out.setResponse(response);		
		return out;
	}
	
	@GetMapping(value="findOne/{id}")
	public ServiceResponse getFindOne(@PathVariable(value="id") String id) {
		
		
		ServiceResponse out = new ServiceResponse();
		List<RequestMapDTO> response = virtualService.findOne(id);		
		out.setResponse(response);		
		return out;
		
		
	}
	
	@GetMapping("/remove/{id}")
	public void remove(@PathVariable(value="id") String id) {
		virtualService.deleteById(id);
	}
	
	@PostMapping("/add")
	public ServiceResponse add(@RequestBody RequestMapDTO requestMapDTO){
		String status = virtualService.save(requestMapDTO);
		ServiceResponse sr = new ServiceResponse();
		sr.setResponse(status);
		return sr;
	}
}
